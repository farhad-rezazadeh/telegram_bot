import requests
from bs4 import BeautifulSoup

"""How It Work
        ReligiousTime Bot use Time.ir website to 
        extract Religious time of the day
    """


def time():

    link = "https://time.ir"
    page = requests.get(link)
    soup = BeautifulSoup(page.text, "html.parser")
    times = [_.getText().replace(' ', '') for _ in soup.select(".text-nowrap")]
    return {"morning_azan": times[0], "sunrise": times[1], "noon_azan": times[2],
            "sunset": times[3], "dusk_azan": times[4], "midnight": times[5]}
