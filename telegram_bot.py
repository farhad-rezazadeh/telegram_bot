import logging
import telegram
from telegram.ext import MessageHandler, Filters, InlineQueryHandler, Updater, CommandHandler, ConversationHandler
from telegram import InlineQueryResultArticle, InputTextMessageContent, ReplyKeyboardMarkup
from get_time import time
# -*- coding: utf-8 -*-


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


# make_message used to create message that include religious time


def make_message(times: dict) -> str:
    message = []

    message.append(" اذان صبح 🕌:{}".format(times.get("morning_azan")))
    message.append(" طلوع آفتاب 🌅:{}".format(times.get("sunrise")))
    message.append(" اذان ظهر 🕌:{}".format(times.get("noon_azan")))
    message.append(" غروب آفتاب 🌇:{}".format(times.get("sunset")))
    message.append(" اذان مغرب 🕌:{}".format(times.get("dusk_azan")),)
    message.append(" نیمه شب 🌓:{}".format(times.get("midnight")))

    message = "\n".join(message)
    return message


def start(update, context):
    reply_keyboard = [['اوقات شرعی']]
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="سلام!من میتونم بهت اوقات شرعی رو نشون بدم",
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

# echo use to show Religious Time it also can handle None command message


def echo(update, context):

    if (update.message.text == "اوقات شرعی"):
        religious_time = make_message(time())
        context.bot.send_message(
            chat_id=update.effective_chat.id,  text=religious_time)
        return
    context.bot.send_message(
        chat_id=update.effective_chat.id, text="پیام معتبر نیست!")


# inline use when you want to use some feature of Bot in another chat

def inline_time(update, context):
    results = list()
    results.append(
        InlineQueryResultArticle(
            id="religious_time",
            title='religious_time',
            religious_time="time",
            input_message_content=InputTextMessageContent("اوقات شرعی")
        )
    )
    context.bot.answer_inline_query(update.inline_query.id, results)


def unknown(update, context):
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="دستور معتبر نیست!!")


"""Logic Of Bot
    The Updater class continuously fetches new updates from telegram 
    and passes them on to the Dispatcher class.
    If you create an Updater object, it will create a Dispatcher for you 
    and link them together with a Queue.
    You can then register handlers of different types in the Dispatcher,
    which will sort the updates fetched by the Updater according to the handlers you registered,
    and deliver them to a callback function that you defined.

    More INFo : https://github.com/python-telegram-bot/python-telegram-bot 
                https://python-telegram-bot.readthedocs.io/en/stable/
    """


def main():

    updater = Updater(
        token='TOKEN',
        use_context=True)

    dispatcher = updater.dispatcher

    start_handler = CommandHandler('start', start)
    dispatcher.add_handler(start_handler)

    inline_time_handler = InlineQueryHandler(inline_time)
    dispatcher.add_handler(inline_time_handler)

    echo_handler = MessageHandler(Filters.text & (~Filters.command), echo)
    dispatcher.add_handler(echo_handler)

    unknown_handler = MessageHandler(Filters.command, unknown)
    dispatcher.add_handler(unknown_handler)

    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
